/**
 * ここには到達しない。
 * 適切に switch 文などが書かれているか型レベルでチェックする。
 */
export const unreachable = (_: never): never => {
	throw new Error('unreachable.')
}

/**
 * 要実装なところにとりあえず置いておく用
 */
export const todo = (mes = '未実装です'): never => {
	errorLog(mes)
	throw new Error(mes)
}

export const errorLog = (mes: unknown) => {
	if ('function' === typeof alert && alert.toString().includes('[native code]'))
		alert(mes)
	else console.error(mes)
}
