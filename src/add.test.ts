import { add } from './add'

describe('add', () => {
	it('1 + 2', () => {
		expect(add(1, 2)).toBe(3)
	})
	it('3 + 2', () => {
		expect(add(3, 2)).toBe(5)
	})
	// it('fail', () => {
	// 	expect(add(3, 2)).toBe(3)
	// })
})
