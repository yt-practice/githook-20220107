import { multiply } from './multiply'

describe.skip('multiply', () => {
	it('2 * 4', () => {
		expect(multiply(2, 4)).toBe(8)
	})
	it('5 * 11', () => {
		expect(multiply(5, 11)).toBe(55)
	})
	it('1 * 4', () => {
		expect(multiply(1, 4)).toBe(4)
	})
	it('7 * 9', () => {
		expect(multiply(7, 9)).toBe(63)
	})
	it('10 * 2', () => {
		expect(multiply(10, 2)).toBe(20)
	})
	it('10 * 10', () => {
		expect(multiply(10, 10)).toBe(100)
	})
	it('5 * 5', () => {
		expect(multiply(5, 5)).toBe(25)
	})
	it('3 * 4', () => {
		expect(multiply(3, 4)).toBe(12)
	})
	it('9 * 1', () => {
		expect(multiply(9, 1)).toBe(9)
	})
})
