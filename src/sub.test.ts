import { sub } from './sub'

describe('sub', () => {
	it('4 - 2', () => {
		expect(sub(4, 2)).toBe(2)
	})
	it('5 - 1', () => {
		expect(sub(5, 1)).toBe(4)
	})
})
