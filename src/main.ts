import { add } from './add'

const main = async () => {
	console.log(add(2, 3))
}

main().catch(x => {
	console.error(x)
	process.exit(1)
})
